# Copyright (C) 2016 Transvelo
# This file is distributed under the same license as the Media Center Extensions package.
msgid ""
msgstr ""
"Project-Id-Version: Media Center Extensions 2.3.0\n"
"Report-Msgid-Bugs-To: http://transvelo.freshdesk.com/\n"
"POT-Creation-Date: 2016-04-29 06:08:29+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: MediaCenter POT <mediacenter@transvelo.com>\n"
"X-Generator: grunt-wp-i18n 0.4.9\n"

#: modules/js_composer/config/map.php:17
msgid "Banner"
msgstr ""

#: modules/js_composer/config/map.php:19
msgid "Add a banner to your page."
msgstr ""

#: modules/js_composer/config/map.php:23 modules/js_composer/config/map.php:170
#: modules/js_composer/config/map.php:222
#: modules/js_composer/config/map.php:281
#: modules/js_composer/config/map.php:342
#: modules/js_composer/config/map.php:422
#: modules/js_composer/config/map.php:553
#: modules/js_composer/config/map.php:644
#: modules/js_composer/config/map.php:735
#: modules/js_composer/config/map.php:827
#: modules/js_composer/config/map.php:884
#: modules/js_composer/config/map.php:961
msgid "Media Center Elements"
msgstr ""

#: modules/js_composer/config/map.php:27
msgid "Banner Image"
msgstr ""

#: modules/js_composer/config/map.php:32 modules/js_composer/config/map.php:174
#: modules/js_composer/config/map.php:346
#: modules/js_composer/config/map.php:426
#: modules/js_composer/config/map.php:473
#: modules/js_composer/config/map.php:557
#: modules/js_composer/config/map.php:604
#: modules/js_composer/config/map.php:648
#: modules/js_composer/config/map.php:888
#: modules/js_composer/config/map.php:965
msgid "Title"
msgstr ""

#: modules/js_composer/config/map.php:34
msgid "Enter banner title"
msgstr ""

#: modules/js_composer/config/map.php:39
msgid "Subtitle Text"
msgstr ""

#: modules/js_composer/config/map.php:41
msgid "Enter banner subtitle"
msgstr ""

#: modules/js_composer/config/map.php:45
msgid "Banner Link"
msgstr ""

#: modules/js_composer/config/map.php:47
msgid "Link to banner. Default #"
msgstr ""

#: modules/js_composer/config/map.php:52
msgid "On Click"
msgstr ""

#: modules/js_composer/config/map.php:56
msgid "Open in same page"
msgstr ""

#: modules/js_composer/config/map.php:57
msgid "Open in new page"
msgstr ""

#: modules/js_composer/config/map.php:62
msgid "Animation on banner hover"
msgstr ""

#: modules/js_composer/config/map.php:65 modules/js_composer/config/map.php:683
msgid "No Animation"
msgstr ""

#: modules/js_composer/config/map.php:66
msgid "bounce"
msgstr ""

#: modules/js_composer/config/map.php:67
msgid "flash"
msgstr ""

#: modules/js_composer/config/map.php:68
msgid "pulse"
msgstr ""

#: modules/js_composer/config/map.php:69
msgid "rubberBand"
msgstr ""

#: modules/js_composer/config/map.php:70
msgid "shake"
msgstr ""

#: modules/js_composer/config/map.php:71
msgid "swing"
msgstr ""

#: modules/js_composer/config/map.php:72
msgid "tada"
msgstr ""

#: modules/js_composer/config/map.php:73
msgid "wobble"
msgstr ""

#: modules/js_composer/config/map.php:74
msgid "bounceIn"
msgstr ""

#: modules/js_composer/config/map.php:75
msgid "bounceInDown"
msgstr ""

#: modules/js_composer/config/map.php:76
msgid "bounceInLeft"
msgstr ""

#: modules/js_composer/config/map.php:77
msgid "bounceInRight"
msgstr ""

#: modules/js_composer/config/map.php:78
msgid "bounceInUp"
msgstr ""

#: modules/js_composer/config/map.php:79
msgid "bounceOut"
msgstr ""

#: modules/js_composer/config/map.php:80
msgid "bounceOutDown"
msgstr ""

#: modules/js_composer/config/map.php:81
msgid "bounceOutLeft"
msgstr ""

#: modules/js_composer/config/map.php:82
msgid "bounceOutRight"
msgstr ""

#: modules/js_composer/config/map.php:83
msgid "bounceOutUp"
msgstr ""

#: modules/js_composer/config/map.php:84
msgid "fadeIn"
msgstr ""

#: modules/js_composer/config/map.php:85
msgid "fadeInDown"
msgstr ""

#: modules/js_composer/config/map.php:86
msgid "fadeInDownBig"
msgstr ""

#: modules/js_composer/config/map.php:87
msgid "fadeInLeft"
msgstr ""

#: modules/js_composer/config/map.php:88
msgid "fadeInLeftBig"
msgstr ""

#: modules/js_composer/config/map.php:89
msgid "fadeInRight"
msgstr ""

#: modules/js_composer/config/map.php:90
msgid "fadeInRightBig"
msgstr ""

#: modules/js_composer/config/map.php:91
msgid "fadeInUp"
msgstr ""

#: modules/js_composer/config/map.php:92
msgid "fadeInUpBig"
msgstr ""

#: modules/js_composer/config/map.php:93
msgid "fadeOut"
msgstr ""

#: modules/js_composer/config/map.php:94
msgid "fadeOutDown"
msgstr ""

#: modules/js_composer/config/map.php:95
msgid "fadeOutDownBig"
msgstr ""

#: modules/js_composer/config/map.php:96
msgid "fadeOutLeft"
msgstr ""

#: modules/js_composer/config/map.php:97
msgid "fadeOutLeftBig"
msgstr ""

#: modules/js_composer/config/map.php:98
msgid "fadeOutRight"
msgstr ""

#: modules/js_composer/config/map.php:99
msgid "fadeOutRightBig"
msgstr ""

#: modules/js_composer/config/map.php:100
msgid "fadeOutUp"
msgstr ""

#: modules/js_composer/config/map.php:101
msgid "fadeOutUpBig"
msgstr ""

#: modules/js_composer/config/map.php:102
msgid "flip"
msgstr ""

#: modules/js_composer/config/map.php:103
msgid "flipInX"
msgstr ""

#: modules/js_composer/config/map.php:104
msgid "flipInY"
msgstr ""

#: modules/js_composer/config/map.php:105
msgid "flipOutX"
msgstr ""

#: modules/js_composer/config/map.php:106
msgid "flipOutY"
msgstr ""

#: modules/js_composer/config/map.php:107
msgid "lightSpeedIn"
msgstr ""

#: modules/js_composer/config/map.php:108
msgid "lightSpeedOut"
msgstr ""

#: modules/js_composer/config/map.php:109
msgid "rotateIn"
msgstr ""

#: modules/js_composer/config/map.php:110
msgid "rotateInDownLeft"
msgstr ""

#: modules/js_composer/config/map.php:111
msgid "rotateInDownRight"
msgstr ""

#: modules/js_composer/config/map.php:112
msgid "rotateInUpLeft"
msgstr ""

#: modules/js_composer/config/map.php:113
msgid "rotateInUpRight"
msgstr ""

#: modules/js_composer/config/map.php:114
msgid "rotateOut"
msgstr ""

#: modules/js_composer/config/map.php:115
msgid "rotateOutDownLeft"
msgstr ""

#: modules/js_composer/config/map.php:116
msgid "rotateOutDownRight"
msgstr ""

#: modules/js_composer/config/map.php:117
msgid "rotateOutUpLeft"
msgstr ""

#: modules/js_composer/config/map.php:118
msgid "rotateOutUpRight"
msgstr ""

#: modules/js_composer/config/map.php:119
msgid "hinge"
msgstr ""

#: modules/js_composer/config/map.php:120
msgid "rollIn"
msgstr ""

#: modules/js_composer/config/map.php:121
msgid "rollOut"
msgstr ""

#: modules/js_composer/config/map.php:122
msgid "zoomIn"
msgstr ""

#: modules/js_composer/config/map.php:123
msgid "zoomInDown"
msgstr ""

#: modules/js_composer/config/map.php:124
msgid "zoomInLeft"
msgstr ""

#: modules/js_composer/config/map.php:125
msgid "zoomInRight"
msgstr ""

#: modules/js_composer/config/map.php:126
msgid "zoomInUp"
msgstr ""

#: modules/js_composer/config/map.php:127
msgid "zoomOut"
msgstr ""

#: modules/js_composer/config/map.php:128
msgid "zoomOutDown"
msgstr ""

#: modules/js_composer/config/map.php:129
msgid "zoomOutLeft"
msgstr ""

#: modules/js_composer/config/map.php:130
msgid "zoomOutRight"
msgstr ""

#: modules/js_composer/config/map.php:131
msgid "zoomOutUp"
msgstr ""

#: modules/js_composer/config/map.php:137
msgid "Banner Text Position"
msgstr ""

#: modules/js_composer/config/map.php:141
msgid "Left"
msgstr ""

#: modules/js_composer/config/map.php:142
msgid "Right"
msgstr ""

#: modules/js_composer/config/map.php:143
msgid "Center"
msgstr ""

#: modules/js_composer/config/map.php:150
#: modules/js_composer/config/map.php:202
#: modules/js_composer/config/map.php:261
#: modules/js_composer/config/map.php:310
#: modules/js_composer/config/map.php:402
#: modules/js_composer/config/map.php:533
#: modules/js_composer/config/map.php:624
#: modules/js_composer/config/map.php:718
#: modules/js_composer/config/map.php:941
msgid "Extra Class"
msgstr ""

#: modules/js_composer/config/map.php:152
#: modules/js_composer/config/map.php:204
#: modules/js_composer/config/map.php:263
#: modules/js_composer/config/map.php:312
#: modules/js_composer/config/map.php:404
#: modules/js_composer/config/map.php:535
#: modules/js_composer/config/map.php:626
#: modules/js_composer/config/map.php:720
#: modules/js_composer/config/map.php:943
msgid "Add your extra classes here."
msgstr ""

#: modules/js_composer/config/map.php:164
msgid "Service Icon"
msgstr ""

#: modules/js_composer/config/map.php:166
msgid "Add a service icon to your page."
msgstr ""

#: modules/js_composer/config/map.php:176
msgid "Enter service title"
msgstr ""

#: modules/js_composer/config/map.php:181
msgid "Service Icon Link"
msgstr ""

#: modules/js_composer/config/map.php:183
msgid "Enter service icon link (optional)"
msgstr ""

#: modules/js_composer/config/map.php:188
msgid "Service Description"
msgstr ""

#: modules/js_composer/config/map.php:190
msgid "Enter service description"
msgstr ""

#: modules/js_composer/config/map.php:195
msgid "Service Icon Class"
msgstr ""

#: modules/js_composer/config/map.php:197
#: modules/js_composer/config/map.php:657
msgid ""
"Fontawesome Icon Class. Default icon is <em>fa-list</em>. For complete list "
"of icon classes %s"
msgstr ""

#: modules/js_composer/config/map.php:197
#: modules/js_composer/config/map.php:657
msgid "Click here"
msgstr ""

#: modules/js_composer/config/map.php:216
msgid "Team Member"
msgstr ""

#: modules/js_composer/config/map.php:218
msgid "Add a team member profile to your page."
msgstr ""

#: modules/js_composer/config/map.php:226
msgid "Full Name"
msgstr ""

#: modules/js_composer/config/map.php:228
msgid "Enter team member full name"
msgstr ""

#: modules/js_composer/config/map.php:233
msgid "Designation"
msgstr ""

#: modules/js_composer/config/map.php:235
msgid "Enter designation of team member"
msgstr ""

#: modules/js_composer/config/map.php:239
msgid "Profile Pic"
msgstr ""

#: modules/js_composer/config/map.php:244
msgid "Display Style"
msgstr ""

#: modules/js_composer/config/map.php:246
msgid "Square"
msgstr ""

#: modules/js_composer/config/map.php:247
msgid "Circle"
msgstr ""

#: modules/js_composer/config/map.php:254
msgid "Link"
msgstr ""

#: modules/js_composer/config/map.php:256
msgid "Add link to the team member. Leave blank if there aren't any"
msgstr ""

#: modules/js_composer/config/map.php:275
msgid "Google Map"
msgstr ""

#: modules/js_composer/config/map.php:277
msgid "Add a Google Map to your page."
msgstr ""

#: modules/js_composer/config/map.php:285
msgid "Latitude"
msgstr ""

#: modules/js_composer/config/map.php:291
msgid "Longitude"
msgstr ""

#: modules/js_composer/config/map.php:297
msgid "Zoom"
msgstr ""

#: modules/js_composer/config/map.php:303
msgid "Minimum Height in px"
msgstr ""

#: modules/js_composer/config/map.php:316
msgid "Display Get Direction"
msgstr ""

#: modules/js_composer/config/map.php:320
#: modules/js_composer/config/map.php:395
#: modules/js_composer/config/map.php:527
#: modules/js_composer/config/map.php:906
#: modules/js_composer/config/map.php:935
msgid "Yes"
msgstr ""

#: modules/js_composer/config/map.php:321
#: modules/js_composer/config/map.php:394
#: modules/js_composer/config/map.php:526
#: modules/js_composer/config/map.php:934
msgid "No"
msgstr ""

#: modules/js_composer/config/map.php:323
msgid "Should display \"Get Direction\" form ?"
msgstr ""

#: modules/js_composer/config/map.php:336
msgid "Brands Carousel"
msgstr ""

#: modules/js_composer/config/map.php:338
msgid "Add a brands carousel to your page"
msgstr ""

#: modules/js_composer/config/map.php:348
#: modules/js_composer/config/map.php:428
#: modules/js_composer/config/map.php:559
#: modules/js_composer/config/map.php:890
msgid "Enter Carousel title"
msgstr ""

#: modules/js_composer/config/map.php:354
msgid "Order by"
msgstr ""

#: modules/js_composer/config/map.php:356
msgid ""
" Sort retrieved posts by parameter. Defaults to 'date'. One or more options "
"can be passed"
msgstr ""

#: modules/js_composer/config/map.php:363
msgid "Order"
msgstr ""

#: modules/js_composer/config/map.php:365
msgid ""
"Designates the ascending or descending order of the 'orderby' parameter. "
"Defaults to 'DESC'."
msgstr ""

#: modules/js_composer/config/map.php:371
msgid "Number of Brands to display"
msgstr ""

#: modules/js_composer/config/map.php:378
#: modules/js_composer/config/map.php:499
msgid "Container Class"
msgstr ""

#: modules/js_composer/config/map.php:382
#: modules/js_composer/config/map.php:503
msgid "Container"
msgstr ""

#: modules/js_composer/config/map.php:383
#: modules/js_composer/config/map.php:504
msgid "Container Fluid"
msgstr ""

#: modules/js_composer/config/map.php:384
#: modules/js_composer/config/map.php:505
msgid "No Container"
msgstr ""

#: modules/js_composer/config/map.php:390
msgid "Should Autoplay"
msgstr ""

#: modules/js_composer/config/map.php:416
msgid "Products Carousel"
msgstr ""

#: modules/js_composer/config/map.php:418
#: modules/js_composer/config/map.php:880
msgid "Add products carousel to your page"
msgstr ""

#: modules/js_composer/config/map.php:433
#: modules/js_composer/config/map.php:564
msgid "Show"
msgstr ""

#: modules/js_composer/config/map.php:437
#: modules/js_composer/config/map.php:568
#: modules/js_composer/config/map.php:753
#: modules/js_composer/config/map.php:773
#: modules/js_composer/config/map.php:793
msgid "Recent Products"
msgstr ""

#: modules/js_composer/config/map.php:438
#: modules/js_composer/config/map.php:569
#: modules/js_composer/config/map.php:750
#: modules/js_composer/config/map.php:770
#: modules/js_composer/config/map.php:790
msgid "Featured Products"
msgstr ""

#: modules/js_composer/config/map.php:439
#: modules/js_composer/config/map.php:570
#: modules/js_composer/config/map.php:752
#: modules/js_composer/config/map.php:772
#: modules/js_composer/config/map.php:792
msgid "Top Rated Products"
msgstr ""

#: modules/js_composer/config/map.php:440
#: modules/js_composer/config/map.php:571
#: modules/js_composer/config/map.php:751
#: modules/js_composer/config/map.php:771
#: modules/js_composer/config/map.php:791
msgid "On Sale Products"
msgstr ""

#: modules/js_composer/config/map.php:441
#: modules/js_composer/config/map.php:572
#: modules/js_composer/config/map.php:754
#: modules/js_composer/config/map.php:774
#: modules/js_composer/config/map.php:794
msgid "Best Selling Products"
msgstr ""

#: modules/js_composer/config/map.php:442
#: modules/js_composer/config/map.php:573
msgid "Select Products by Category"
msgstr ""

#: modules/js_composer/config/map.php:443
#: modules/js_composer/config/map.php:574
msgid "Select Products by IDs"
msgstr ""

#: modules/js_composer/config/map.php:444
#: modules/js_composer/config/map.php:575
msgid "Select Products by SKUs"
msgstr ""

#: modules/js_composer/config/map.php:446
#: modules/js_composer/config/map.php:577
msgid "Choose what type of products you want to show in the carousel"
msgstr ""

#: modules/js_composer/config/map.php:450
#: modules/js_composer/config/map.php:581
msgid "IDs"
msgstr ""

#: modules/js_composer/config/map.php:452
#: modules/js_composer/config/map.php:583
msgid ""
"Note : This option is applicable only for Select Products by IDs. Specify "
"IDs of products you want to show separated by comma. Example : 1, 2, 3, 4, 5"
msgstr ""

#: modules/js_composer/config/map.php:456
#: modules/js_composer/config/map.php:587
msgid "SKUs"
msgstr ""

#: modules/js_composer/config/map.php:458
#: modules/js_composer/config/map.php:589
msgid ""
"Note : This option is applicable only for Select Products by SKUs. Specify "
"SKUs of products you want to show separated by comma. Example : foo, bar, "
"baz"
msgstr ""

#: modules/js_composer/config/map.php:462
#: modules/js_composer/config/map.php:593
#: modules/js_composer/config/map.php:911
msgid "Category"
msgstr ""

#: modules/js_composer/config/map.php:464
#: modules/js_composer/config/map.php:595
msgid ""
"Note : This option is applicable only for Select Products by Category. "
"Specify the category slug of the category of products you want to show"
msgstr ""

#: modules/js_composer/config/map.php:468
#: modules/js_composer/config/map.php:599
msgid "Order By"
msgstr ""

#: modules/js_composer/config/map.php:472
#: modules/js_composer/config/map.php:603
msgid "Menu Order"
msgstr ""

#: modules/js_composer/config/map.php:474
#: modules/js_composer/config/map.php:605
msgid "Date"
msgstr ""

#: modules/js_composer/config/map.php:475
#: modules/js_composer/config/map.php:606
msgid "Random"
msgstr ""

#: modules/js_composer/config/map.php:476
#: modules/js_composer/config/map.php:607
msgid "ID"
msgstr ""

#: modules/js_composer/config/map.php:478
#: modules/js_composer/config/map.php:489
#: modules/js_composer/config/map.php:609
#: modules/js_composer/config/map.php:620
msgid "Does not apply for Best Selling Products"
msgstr ""

#: modules/js_composer/config/map.php:482
#: modules/js_composer/config/map.php:613
msgid "Order Direction"
msgstr ""

#: modules/js_composer/config/map.php:486
#: modules/js_composer/config/map.php:617
msgid "Descending"
msgstr ""

#: modules/js_composer/config/map.php:487
#: modules/js_composer/config/map.php:618
msgid "Ascending"
msgstr ""

#: modules/js_composer/config/map.php:493
#: modules/js_composer/config/map.php:896
msgid "No of Products"
msgstr ""

#: modules/js_composer/config/map.php:511
#: modules/js_composer/config/map.php:919
msgid "No of Columns"
msgstr ""

#: modules/js_composer/config/map.php:515
#: modules/js_composer/config/map.php:923
msgid "4 - Best suited for pages with sidebar"
msgstr ""

#: modules/js_composer/config/map.php:516
#: modules/js_composer/config/map.php:924
msgid "6 - Best suited for Full-width pages"
msgstr ""

#: modules/js_composer/config/map.php:522
#: modules/js_composer/config/map.php:930
msgid "Should Autoplay ?"
msgstr ""

#: modules/js_composer/config/map.php:547
msgid "6-1 Products Grid"
msgstr ""

#: modules/js_composer/config/map.php:549
msgid "Add 6-1 Products Grid to your page"
msgstr ""

#: modules/js_composer/config/map.php:638
msgid "Vertical Menu"
msgstr ""

#: modules/js_composer/config/map.php:640
msgid "Add a vertical menu to your home page"
msgstr ""

#: modules/js_composer/config/map.php:655
msgid "Title Icon Class"
msgstr ""

#: modules/js_composer/config/map.php:662
msgid "Menu"
msgstr ""

#: modules/js_composer/config/map.php:664
msgid "Menu ID, slug, or name. Leave it empty to pull all categories"
msgstr ""

#: modules/js_composer/config/map.php:669
msgid "Dropdown Trigger"
msgstr ""

#: modules/js_composer/config/map.php:673
msgid "Click"
msgstr ""

#: modules/js_composer/config/map.php:674
msgid "Hover"
msgstr ""

#: modules/js_composer/config/map.php:680
msgid "Dropdown Animation"
msgstr ""

#: modules/js_composer/config/map.php:684
msgid "BounceIn"
msgstr ""

#: modules/js_composer/config/map.php:685
msgid "BounceInDown"
msgstr ""

#: modules/js_composer/config/map.php:686
msgid "BounceInLeft"
msgstr ""

#: modules/js_composer/config/map.php:687
msgid "BounceInRight"
msgstr ""

#: modules/js_composer/config/map.php:688
msgid "BounceInUp"
msgstr ""

#: modules/js_composer/config/map.php:689
msgid "FadeIn"
msgstr ""

#: modules/js_composer/config/map.php:690
msgid "FadeInDown"
msgstr ""

#: modules/js_composer/config/map.php:691
msgid "FadeInDown Big"
msgstr ""

#: modules/js_composer/config/map.php:692
msgid "FadeInLeft"
msgstr ""

#: modules/js_composer/config/map.php:693
msgid "FadeInLeft Big"
msgstr ""

#: modules/js_composer/config/map.php:694
msgid "FadeInRight"
msgstr ""

#: modules/js_composer/config/map.php:695
msgid "FadeInRight Big"
msgstr ""

#: modules/js_composer/config/map.php:696
msgid "FadeInUp"
msgstr ""

#: modules/js_composer/config/map.php:697
msgid "FadeInUp Big"
msgstr ""

#: modules/js_composer/config/map.php:698
msgid "FlipInX"
msgstr ""

#: modules/js_composer/config/map.php:699
msgid "FlipInY"
msgstr ""

#: modules/js_composer/config/map.php:700
msgid "Light SpeedIn"
msgstr ""

#: modules/js_composer/config/map.php:701
msgid "RotateIn"
msgstr ""

#: modules/js_composer/config/map.php:702
msgid "RotateInDown Left"
msgstr ""

#: modules/js_composer/config/map.php:703
msgid "RotateInDown Right"
msgstr ""

#: modules/js_composer/config/map.php:704
msgid "RotateInUp Left"
msgstr ""

#: modules/js_composer/config/map.php:705
msgid "RotateInUp Right"
msgstr ""

#: modules/js_composer/config/map.php:706
msgid "RoleIn"
msgstr ""

#: modules/js_composer/config/map.php:707
msgid "ZoomIn"
msgstr ""

#: modules/js_composer/config/map.php:708
msgid "ZoomInDown"
msgstr ""

#: modules/js_composer/config/map.php:709
msgid "ZoomInLeft"
msgstr ""

#: modules/js_composer/config/map.php:710
msgid "ZoomInRight"
msgstr ""

#: modules/js_composer/config/map.php:711
msgid "ZoomInUp"
msgstr ""

#: modules/js_composer/config/map.php:732
msgid "Home Page Tabs"
msgstr ""

#: modules/js_composer/config/map.php:734
#: modules/js_composer/config/map.php:826
msgid "Product Tabs for Home"
msgstr ""

#: modules/js_composer/config/map.php:740
#: modules/js_composer/config/map.php:832
msgid "Tab #1 title"
msgstr ""

#: modules/js_composer/config/map.php:746
#: modules/js_composer/config/map.php:838
msgid "Tab #1 Content, Show :"
msgstr ""

#: modules/js_composer/config/map.php:760
#: modules/js_composer/config/map.php:845
msgid "Tab #2 title"
msgstr ""

#: modules/js_composer/config/map.php:766
#: modules/js_composer/config/map.php:851
msgid "Tab #2 Content, Show :"
msgstr ""

#: modules/js_composer/config/map.php:780
#: modules/js_composer/config/map.php:858
msgid "Tab #3 title"
msgstr ""

#: modules/js_composer/config/map.php:786
#: modules/js_composer/config/map.php:864
msgid "Tab #3 Content, Show :"
msgstr ""

#: modules/js_composer/config/map.php:824
msgid "Home Page Tabs for ECWID"
msgstr ""

#: modules/js_composer/config/map.php:878
msgid "Products Carousel for ECWID"
msgstr ""

#: modules/js_composer/config/map.php:903
msgid "Category Products"
msgstr ""

#: modules/js_composer/config/map.php:904
msgid "If unchecked displays random products."
msgstr ""

#: modules/js_composer/config/map.php:912
msgid "Only when checked category products."
msgstr ""

#: modules/js_composer/config/map.php:955
msgid "Vertical Categories for ECWID"
msgstr ""

#: modules/js_composer/config/map.php:957
msgid "Add vertical categories to your page"
msgstr ""

#: modules/js_composer/config/map.php:967
msgid "Enter title"
msgstr ""

#: modules/js_composer/include/shortcodes/shortcode_mc_gmap.php:50
msgid "Enter Your Starting Point"
msgstr ""

#: modules/js_composer/include/shortcodes/shortcode_mc_gmap.php:52
msgid "Get Directions"
msgstr ""

#: modules/js_composer/include/shortcodes/shortcode_mc_vertical_menu.php:16
msgid "All Departments"
msgstr ""

#: modules/js_composer/js_composer.php:94
msgid ""
"<strong>%s</strong> requires <strong><a href=\"http://bit.ly/vcomposer\" "
"target=\"_blank\">Visual Composer</a></strong> plugin to be installed and "
"activated on your site."
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:61
msgid "Add New Block"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:62
msgid "Edit Block"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:63
msgid "New Block"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:64
msgid "Static Blocks"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:65
msgid "View Block"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:66
msgid "Search"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:67
msgid "No blocks found"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:68
msgid "No blocks found in Trash"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:192
msgid "Content Options"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:198
msgid "Content Filters"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:199
msgid "Apply all WP content filters? This will include plugin added filters."
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:204
msgid "Defaults (recommended)"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:205
msgid "All Content Filters"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:209
msgid "Auto Paragraphs"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:210
msgid ""
"Add &lt;p&gt; and &lt;br&gt; tags automatically.<br>(disabling may fix "
"layout issues)"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:215
msgid "On"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:216
msgid "Off"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:73
msgid "Search Brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:74
msgid "Popular Brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:75
msgid "All Brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:78
msgid "Edit Brand"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:79
msgid "Update Brand"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:80
msgid "Add New Brand"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:81
msgid "New Brand Name"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:82
msgid "Separate brands with commas"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:83
msgid "Add or remove brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:84
msgid "Choose from the most used brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:85
msgid "No brands found."
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:86
msgid "Brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:106
msgid "Search Labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:107
msgid "Popular Labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:108
msgid "All Labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:111
msgid "Edit Label"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:112
msgid "Update Label"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:113
msgid "Add New Label"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:114
msgid "New Label Name"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:115
msgid "Separate labels with commas"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:116
msgid "Add or remove labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:117
msgid "Choose from the most used labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:118
msgid "No labels found."
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:119
msgid "Labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:180
#: modules/product-taxonomies/class-mc-product-taxonomies.php:258
msgid "Thumbnail"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:184
#: modules/product-taxonomies/class-mc-product-taxonomies.php:263
msgid "Upload/Add image"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:185
#: modules/product-taxonomies/class-mc-product-taxonomies.php:264
msgid "Remove image"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:208
#: modules/product-taxonomies/class-mc-product-taxonomies.php:283
msgid "Choose an image"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:210
#: modules/product-taxonomies/class-mc-product-taxonomies.php:285
msgid "Use image"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:344
msgid "Image"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:394
#: modules/product-taxonomies/class-mc-product-taxonomies.php:419
msgid "Background Color"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:396
msgid ""
"Background color as a hex value. For example : #000000 is black and #FFFFFF "
"is white"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:399
#: modules/product-taxonomies/class-mc-product-taxonomies.php:425
#: modules/product-taxonomies/class-mc-product-taxonomies.php:463
msgid "Text Color"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:401
msgid ""
"Text color as a hex value. For example : #000000 is black and #FFFFFF is "
"white"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:462
msgid "BG Color"
msgstr ""

#: modules/theme-shortcodes/compare.php:13
msgid "You need to enable YITH Compare plugin for product comparison to work"
msgstr ""

#: modules/theme-shortcodes/compare.php:23
msgid "Compare"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Media Center Extensions"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://demo.transvelo.com/media-center-wp/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Extensions for Media Center Wordpress Theme. Supplied as a separate plugin "
"so that the customer does not find empty shortcodes on changing the theme."
msgstr ""

#. Author of the plugin/theme
msgid "Transvelo"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://transvelo.com/"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:58
msgctxt "post type general name"
msgid "Static Content Blocks"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:59
msgctxt "post type singular name"
msgid "Static Block"
msgstr ""

#: modules/post-type-static-block/post-type-static-block.php:60
msgctxt "block"
msgid "Add New"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:71
msgctxt "taxonomy general name"
msgid "Brands"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:104
msgctxt "taxonomy general name"
msgid "Labels"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:72
msgctxt "taxonomy singular name"
msgid "Brand"
msgstr ""

#: modules/product-taxonomies/class-mc-product-taxonomies.php:105
msgctxt "taxonomy singular name"
msgid "Label"
msgstr ""